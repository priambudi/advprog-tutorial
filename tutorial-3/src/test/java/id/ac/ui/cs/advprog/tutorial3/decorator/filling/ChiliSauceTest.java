package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import org.junit.Before;
import org.junit.Test;

public class ChiliSauceTest {
    private Cheese.ChiliSauce thickBunChiliSauceBurger;

    @Before
    public void setUp() {
        thickBunChiliSauceBurger = new Cheese.ChiliSauce(new ThickBunBurger());
    }

    @Test
    public void testMethodCost() {
        assertEquals(2.80, thickBunChiliSauceBurger.cost(), 0.00);
    }

    @Test
    public void testMethodGetDescription() {
        assertEquals("Thick Bun Burger, adding chili sauce",
                thickBunChiliSauceBurger.getDescription());
    }
}
